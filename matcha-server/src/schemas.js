const schemas = {
    user: {
        id: null,
        email: null,
        password: null,
        username: null,
        created_at: null,
        updated_at: null
    }
}

export default schemas;
